# Password Generator #

Version 2.0.1

A password generator written in bash.

## Installation ##

To install this script, the only thing you have to do is to put it under `/usr/bin` or `/usr/local/bin` if you want to use it system wide or under `~/bin` or `~/.local/bin` for your user only.

## Usage ##

### Command line arguments ###

	-h, --help           Print this help text and exit.
	-v, --version        Print program version and exit.
	-C, --clear-memory   Clear clipboard and temporary memory from generated passwords.
	--clear              Clear character groups to be used.
	--no-color           Don't colorize the output.
	--list               Print a list of character groups.
	--length[=]<number>  Set the desired lengh for the password.
	-c, --clip           Copy generated password to clipboard.
	-o, --no-output      Don't print generated password to output.
	+<groupNumber>       Add a character group by it's group number.
	+<groupSName>        Add a character group by it's group short name.
	-<groupNumber>       Remove a character group by it's group number.
	-<groupSName>        Remove a character group by it's group short name.

### Character groups ###

This is the output of the `passgen --list` command.

	1	enlo	English lower case
	2	enup	English upper case
	3	numb	Numbers
	4	spc1	Special characters 1
	5	spc2	Special characters 2
	6	spcs	Spaces
	7	urlu	URL unsafe
	8	html	html/xml unsafe
	9	grlo	Greek lower case no accents
	10	grup	Greek upper case no accents
	11	grla	Greek lower case accents only
	12	grua	Greek upper case accents only
	13	des1	German special I
	14	des2	German special II
	15	crnc	Currencies symbols
	16	qmrk	Quotation marks
	17	spec	Special

### "settings.sh" file ###

This file can be used for storing settings of passgen. Valid locations of the file can be "~/.local/etc/passgen/settings.sh" and "/etc/passgen/settings.sh", with read permissions by the user.

In the file the following variables and functions can be used to configure passgen:

#### passLength ####

Sets the default password length.

Must be an integer with value greater than zero. Default value is 20.

Example:
```bash
passLength=20
```

#### selectGroups ####

Sets the default character groups to be enabled.

This must be a string having the same length in characters as the number of characterGroups. To select a character group, there must be the character "1" at the coresponding position in string. All the other characters of the string must be "0".

The default value is "11110000000000000"

Example:
```bash
selectGroups="11000000000000000" # this selects the first two character groups (english lower case and english upper case characters)
```

#### charGroupNames ####

This is an array of the descriptions of the character groups.

#### charGroupSortNames ####

This is an array of the sort names of the character groups.

#### charGroups ####

This is an array of the character groups.

#### pcol ####

This variable is for stylizing password on output.

#### coloredoutput ####

Sets if output will be stylized.

#### copytoclipboard ####

Sets if generated password will be copyed to clipboard by default.

#### clearafter ####

The number in minutes to clear the clipboard if contents matching with the last generated password. If value is "false" then the clipboard won't be cleared.

#### nooutput ####

"true" for not displaying generated password, "false" otherwise.

### Examples ###

Generate a random password using default settings.
```bash
$ passgen
```

Generate a random password of 20 character length.
```bash
$ passgen --length 20
```

Generate a random password of 25 characters that contains only lower case english character and numbers.
```bash
$ passgen --length 25 --clear +enlo +numb
```

Print a table showing character groups.
```bash
$ passgen --list
```
Generate a password without displaying it, copy it to clipboard and clear clipboard when five minutes passes.
```bash
$ passgen -c -o
```
